pragma solidity ^0.5.14;
pragma experimental ABIEncoderV2;

import {IBridge} from "./interface/IBridge.sol";
import {ParamsDecoder, ResultDecoder} from "./lib/Decoders.sol";
import "openzeppelin-solidity/contracts/math/SafeMath.sol";

contract VRF {
    using SafeMath for uint256;
    using ResultDecoder for bytes;
    using ParamsDecoder for bytes;

    IBridge public bridge;
    string public number;

    constructor(
        IBridge bridge_
    ) public {
        bridge = bridge_;
    }

    function verifyAndSaveRandomNumber(bytes memory _data) public {
        (
            IBridge.RequestPacket memory latestReq,
            IBridge.ResponsePacket memory latestRes
        ) = bridge.relayAndVerify(_data);

        ParamsDecoder.Params memory params = latestReq.params.decodeParams();
        ResultDecoder.Result memory result = latestRes.result.decodeResult();

        require(
            params.size ==
                1,
            "ERROR_MULTIPLIER_DOES_NOT_MATCH_WITH_EXPECTED"
        );

        number = result.random_bytes;
    }

}